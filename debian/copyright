Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: linbox
Source: http://linalg.org/

Files: *
Copyright: Linbox Team <linbox-use@googlegroups.com>
           Givaro Team
           Jean-Guillaume Dumas <Jean-Guillaume.Dumas@imag.fr>
           Mark Giesbrecht <mwg@csd.uwo.ca>
           Pascal Giorgi <Pascal.Giorgi@lirmm.fr>
           Bradford Hovinen <hovinen@cis.udel.edu>
           Erich Kaltofen <kaltofen@math.ncsu.edu>
           Clement Pernet <Clement.Pernet@imag.fr>
           Daniel Roche <roche@cis.udel.edu>
           B. David Saunders <saunders@cis.udel.edu>
           Arne Storjohann <storjoha@inf.ethz.ch>
           William Turner <turnerw@wabash.edu>
           Gilles Villard <Gilles.Villard@ens-lyon.fr>
           Zhendong Wan <wan@cis.udel.edu>
License: LGPL-2.1+

Files: linbox/algorithms/block-coppersmith-domain.h
Copyright: 2012 George Yuhasz
License: LGPL-2.1+

Files: linbox/algorithms/diophantine-solver.h
       linbox/algorithms/diophantine-solver.inl
       linbox/algorithms/vector-fraction.h
Copyright: David Pritchard
License: LGPL-2.1+

Files: linbox/blackbox/hilbert.h
Copyright: John P. May
           B. David Saunders
License: LGPL-2.1+

Files: linbox/algorithms/charpoly-rational.h
       linbox/algorithms/classic-rational-reconstruction.h
       linbox/algorithms/cra-builder-var-prec-early-multip.h
       linbox/algorithms/cra-builder-var-prec-early-single.h
       linbox/algorithms/det-rational.h
       linbox/algorithms/fast-rational-reconstruction.h
       linbox/algorithms/minpoly-rational.h
       linbox/algorithms/rational-cra-var-prec.h
       linbox/algorithms/rational-reconstruction-base.h
       linbox/blackbox/rational-matrix-factory.h
Copyright: 2009 Anna Marszalek <aniau@astronet.pl>
License: LGPL-2.1+

Files: linbox/algorithms/hybrid-det.h
Copyright: 2005 Anna Urbanska
License: LGPL-2.1+

Files: linbox/blackbox/ntl-hankel.h
       linbox/blackbox/ntl-hankel.inl
       linbox/blackbox/ntl-sylvester.h
       linbox/blackbox/ntl-sylvester.inl
Copyright: 2003 Austin Lobo
           2003 B. David Saunders
License: LGPL-2.1+

Files: linbox/blackbox/toeplitz.h
       linbox/blackbox/toeplitz.inl
       tests/test-ntl-toeplitz.C
Copyright: 2002 Austin Lobo
           2002 B. David Saunders
License: LGPL-2.1+

Files: linbox/blackbox/zero-one.h
       linbox/blackbox/zero-one.inl
       linbox/blackbox/zo.inl
Copyright: 2002 Rich Seagraves
License: LGPL-2.1+

Files: linbox/randiter/mersenne-twister.C
Copyright: 1997 Makoto Matsumoto
           1997 Takuji Nishimura
           1998 Shawn J. Cokus
License: LGPL-2.1+

Files: debian/*
Copyright: 2011 Lifeng Sun <lifongsun@gmail.com>
           2016-2024 Doug Torrance <dtorrance@debian.org>
License: LGPL-2.1+

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".
